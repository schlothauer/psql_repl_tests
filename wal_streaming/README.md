# Simple master/slave streaming replication

## Steps to configure
### Configure master
1. install postgresql
2. halt postgresql
3. prepare for WAL archives (postgresql.conf)
4. add a replication user
5. set login/auth configuration for replication user (pg_hba.conf)
6. start postgresql

```bash
# postgresql.conf
wal_buffers = 8MB
wal_level = hot_standby
max_wal_senders = 3
wal_keep_segments = 8
archive_mode = on
archive_command = 'test ! -f $PGDATA/$REPLICATION_ARCHIV_PATH/%f && cp %p $PGDATA/$REPLICATION_ARCHIV_PATH/%f'
```

```sql
# add replication user
CREATE USER "$REPLICATION_USER" REPLICATION LOGIN PASSWORD '$REPLICATION_PASSWORD';
```

```bash
# pg_hba.conf
host    replication     $REPLICATION_USER      0.0.0.0/0            md5
```

### Configure slave
1. install postgresql
2. halt postgresql
3. $PGDATA backup and creation of new and empty $PGDATA
4. initial copy from master server
5. create recovery configuration (recovery.conf)
6. modify configuration for hot_standby (postgresql.conf)
7. start postgresql

```bash
dataDirName=${PGDATA##*/}            
cd $PGDATA/..
#step 3
mv $dataDirName ${dataDirName}.orig
mkdir $PGDATA
chown -R postgres $PGDATA
chmod -R 700 "$PGDATA"
# step 4
PGPASSWORD=$REPLICATION_PASSWORD pg_basebackup -h $REPLICATION_MASTER -D $PGDATA -U $REPLICATION_USER -v 
chown -R postgres $dataDirName
# step 5
echo "
#------------------------------------------------------------------------------
# Recovery configuration
#------------------------------------------------------------------------------
standby_mode = 'on'
primary_conninfo = 'host=$REPLICATION_MASTER port=5432 user=$REPLICATION_USER password=$REPLICATION_PASSWORD'
restore_command = 'cp $PGDATA/$REPLICATION_ARCHIV_PATH/%f %p'
archive_cleanup_command = 'pg_archivecleanup $PGDATA/$REPLICATION_ARCHIV_PATH %r'
" | tee -a $PGDATA/recovery.conf > /dev/null
            chown postgres $PGDATA/recovery.conf

# step 6
echo "hot_standby = on" >> $dataDirName/postgresql.conf
```

### Test configuration
```bash
# show log entries of the containers
docker logs psql1_master
docker logs psql2_slave

# start psql instances to the database servers
# psql1
docker run -it --rm postgres:9.5 su -c "psql -U anna -h 192.168.178.24 -p 6432 anna"
# psql2
docker run -it --rm postgres:9.5 su -c "psql -U anna -h 192.168.178.24 -p 7432 anna"
``` 
