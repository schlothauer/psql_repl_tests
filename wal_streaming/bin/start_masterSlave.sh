#!/bin/bash

scriptPos=${0%/*}

absPathToBase=$(pushd $scriptPos/.. > /dev/null; pwd ; popd > /dev/null)


composeFile="$scriptPos/../masterSlave/docker-compose.yml"

if ! [ -d $scriptPos/../data/psql1 ]; then mkdir -p $scriptPos/../data/psql1; fi
if ! [ -d $scriptPos/../data/psql2 ]; then mkdir -p $scriptPos/../data/psql2; fi

contName1=psql1_master
contName2=psql1_slave

docker ps -f name="$contName1" | grep "$contName1" > /dev/null && echo -en "\033[1;31m  container seems to be up: $contName1\033[0m\n" && exit 1

docker ps -f name="$contName2" | grep "$contName2" > /dev/null && echo -en "\033[1;31m it seems the oposite master/slave scenario is currently up: $contName2\033[0m\n" && exit 1

if docker ps -a -f name="$contName1" | grep "$contName1" > /dev/null; then
    docker-compose -f "$composeFile" start
else
    docker-compose -f "$composeFile" up -d
fi

