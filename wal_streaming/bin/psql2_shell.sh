#!/bin/bash

contName1=psql2_master
contName2=psql2_slave

if docker ps -f name="$contName1" | grep "$contName1" > /dev/null; then
    docker exec -it $contName1 /bin/bash
else
    if docker ps -f name="$contName2" | grep "$contName2" > /dev/null; then
        docker exec -it $contName2 /bin/bash
    else
        echo "container $contName1 or $contName are not started"
        exit 1        
    fi
fi


