#!/bin/bash

# stop the test environment

scriptPos=${0%/*}

composeFile="$scriptPos/../slaveMaster/docker-compose.yml"

docker-compose -f "$composeFile" down

