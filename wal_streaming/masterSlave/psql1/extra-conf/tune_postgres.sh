#!/bin/bash

if [ $# -eq 0 ]; then
    echo "no PGDATA argument given :-( ... stop"
    exit 1
fi

pgDataDir=$1
echo "pgDataDir: $pgDataDir"

if ! [ -d "$pgDataDir" ]; then
    echo "PGDATA-Param ($pgDataDir) don't points to a directory ... stop"
    exit 1
fi

if ! [ -f "$pgDataDir/postgresql.conf" ]; then
    echo "configuration file doesn't exist: $pgDataDir/postgresql.conf ... stop"
fi

pushd "$pgDataDir" > /dev/null
    sed -r -i.bak \
        -e '/\bwork_mem\b/d' \
        -e '/\bmaintenance_work_mem\b/d' \
        -e '/\bcheckpoint_segments\b/d' \
    -e '/\bcheckpoint_completion_target\b/d' \
        -e '/\bwal_buffers\b/d' \
        postgresql.conf
        echo "
#------------------------------------------------------------------------------
# EXTRA CONFIGURATION
#------------------------------------------------------------------------------

work_mem = 32MB
maintenance_work_mem = 512MB
checkpoint_completion_target = 0.9
wal_buffers = 8MB
wal_level = hot_standby
max_wal_senders = 3
wal_keep_segments = 8
archive_mode = on
archive_command = 'test ! -f $PGDATA/$REPLICATION_ARCHIV_PATH/%f && cp %p $PGDATA/$REPLICATION_ARCHIV_PATH/%f'
" | tee -a postgresql.conf > /dev/null

mkdir $REPLICATION_ARCHIV_PATH;
chown -R postgres $REPLICATION_ARCHIV_PATH

popd > /dev/null

echo "configuration changed: $pgDataDir/postgresql.conf"
